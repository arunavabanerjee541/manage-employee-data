﻿using ManageEmployeeData.Data_Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ManageEmployeeData
{
     public class Manager 
     {
        private IManipulateData _manipulateData;
        private IFetchData _fetchData;

        public Manager()
        {
            _manipulateData = new ManipulateData();
            _fetchData = new FetchData();
        }


        public void AddData(IMyCollection<Employee> myCollection, CollectionType typeOfCollection)
        {
            _manipulateData.AddData(myCollection,typeOfCollection);
        }

        public void RemoveData(IMyCollection<Employee> myCollection, CollectionType typeOfCollection)
        {
            _manipulateData.RemoveData(myCollection,typeOfCollection);
        }

        public void ShowAllEmployeeDetails(IMyCollection<Employee> myCollection)
        {
            _fetchData.FetchAllEmployeeDetails(myCollection);
        }

        public void ShowEmployeeByEmailAddress(IMyCollection<Employee> myCollection)
        {
             Console.WriteLine("please enter the employee email address whose details are needed");
             string mail = Console.ReadLine();
            _fetchData.FetchEmployeeDetailsByEmailAddress(mail, myCollection);
        }


     

     }
}
