﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ManageEmployeeData
{
    public class Employee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal Salary { get; set; }
        public MailAddress EmailAddress { get; set; }


        public Employee(string firstName,string lastName,decimal salary,MailAddress emailAddress)
        {
            FirstName = firstName;
            LastName = lastName;
            Salary = salary;
            EmailAddress = emailAddress;

        }

        public Employee()
        {

        }


    }
}
