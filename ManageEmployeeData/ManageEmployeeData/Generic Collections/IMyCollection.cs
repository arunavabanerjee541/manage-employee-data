﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageEmployeeData
{
    public interface IMyCollection<T>
    {
        Iterator<T> CreateIterator();       
    }
}
