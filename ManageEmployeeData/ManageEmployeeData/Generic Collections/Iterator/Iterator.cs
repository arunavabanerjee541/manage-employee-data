﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageEmployeeData
{
    public interface Iterator<T>
    {
        bool HasNext();
        T Current();
        void Next();

    }
}
