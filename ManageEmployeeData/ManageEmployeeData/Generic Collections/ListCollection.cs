﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageEmployeeData
{
    class ListCollection<T> : IMyCollection<T>
    {
        private List<T> _listOfItems;
        private int _numberOfItems;
        public ListCollection()
        {
            _listOfItems = new List<T>();
        }

        public void Add(T item)
        {
          _listOfItems.Add(item);
            _numberOfItems++;
        }

        public void Remove(int IndexOfItemToBeRemoved)
        {
            _listOfItems.RemoveAt(IndexOfItemToBeRemoved);
            _numberOfItems--;
        }

        public Iterator<T> CreateIterator()
        {
            return new ListIterator(this);
        }

        public class ListIterator : Iterator<T>
        {
            private ListCollection<T> _listCollection;
            private int _index;

            public ListIterator(ListCollection<T> listCollection)
            {
                this._listCollection = listCollection;
            }

            public T Current()
            {
                return _listCollection._listOfItems[_index];
            }

            public bool HasNext()
            {
                return _index < _listCollection._listOfItems.Count;
            }

            public void Next()
            {
                _index++;
            }
        }
       
    }
}
