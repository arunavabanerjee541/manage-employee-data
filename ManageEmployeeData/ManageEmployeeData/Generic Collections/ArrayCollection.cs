﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageEmployeeData
{
    class ArrayCollection<T> : IMyCollection<T>
    {
        private T[] _arrayOfItems;
        private int _numberOfItems;
        private readonly int _maxNumberOfItemsAllowed = 10;

        public ArrayCollection()
        {
            _arrayOfItems = new T[_maxNumberOfItemsAllowed];  
        }

        public void Add(T item)
        {
            _arrayOfItems[_numberOfItems] = item;
            _numberOfItems++;
        }

        public void Remove(int indexToBeRemoved)
        {        
           for(int index=indexToBeRemoved; index<_arrayOfItems.Length-1;index++)
            {
                _arrayOfItems[index] = _arrayOfItems[index + 1];
            }
            _numberOfItems--;
        }

        public Iterator<T> CreateIterator()
        {
            return new ArrayIterator(this);
        }


        public bool IsTotalItemLessThanMaxItemsAllowed()
        {
            return _numberOfItems < _maxNumberOfItemsAllowed;
        }






        public class ArrayIterator : Iterator<T>
        {
            private ArrayCollection<T> _arrayCollection;
            int index = 0;

            public ArrayIterator(ArrayCollection<T> arrayCollection)
            {
                this._arrayCollection = arrayCollection;
            }

            public T Current()
            {
                return _arrayCollection._arrayOfItems[index];
            }

            public bool HasNext()
            {
                return index < _arrayCollection._numberOfItems;
            }

            public void Next()
            {
                index++;
            }
        }
     

    }
}
