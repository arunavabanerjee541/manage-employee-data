﻿using System.Collections.Generic;
using System.Linq;

namespace ManageEmployeeData
{
    public class DictionaryCollection<TKey,TValue> : IMyCollection<TValue> 
       {
        private Dictionary<TKey, TValue> _dictionaryOfItems;
        private int _numberOfItems;
        public DictionaryCollection()
        {     
            _dictionaryOfItems= new Dictionary<TKey, TValue>();          
        }

        public void Add(TKey key,TValue value)
        {
            _dictionaryOfItems.Add(key,value);
            _numberOfItems++;
        }

        public void Remove(int indexOfItemToBeRemoved)
        {
             KeyValuePair<TKey,TValue> itemToBeRemoved =_dictionaryOfItems.ElementAt(indexOfItemToBeRemoved);
            _dictionaryOfItems.Remove(itemToBeRemoved.Key);
            _numberOfItems--;
        }

        public Iterator<TValue> CreateIterator()
        {
            return new DictionaryIterator(this);
        }

        public class DictionaryIterator : Iterator<TValue> 
        {
            private DictionaryCollection<TKey, TValue> _dictionaryCollection;
            int index = 0;

            public DictionaryIterator(DictionaryCollection<TKey, TValue> DictionaryCollection)
            {
                this._dictionaryCollection = DictionaryCollection;
            }

            public TValue Current()
            {
               return _dictionaryCollection._dictionaryOfItems.ElementAt(index).Value;
               
            }

            public bool HasNext()
            {
                return index < _dictionaryCollection
                       ._dictionaryOfItems.Count();
            }

            public void Next()
            {
                index++;
            }
        }

    }
}
