﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageEmployeeData.Data_Management
{
    public delegate bool CheckData<Employee>(string data, IMyCollection<Employee> myCollection);
    interface IValidateData
    {        
     bool IsDataValid(string myData, CheckData<Employee> data, IMyCollection<Employee> myCollection);      
    }
}
