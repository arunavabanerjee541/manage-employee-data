﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageEmployeeData.Data_Management
{
    class Validate:IValidateData 
    {
        public bool IsDataValid(string myData, CheckData<Employee> data, IMyCollection<Employee> myCollection)
        {
            if (data(myData, myCollection))
            {
                return true;
            }
            return false;
        }
    }
}
