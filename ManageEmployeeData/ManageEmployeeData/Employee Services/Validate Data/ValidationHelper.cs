﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ManageEmployeeData.Data_Management
{
    class ValidationHelper
    {
        public bool IsContentOfTheMailValid(string mail, IMyCollection<Employee> myCollection  )
        {
            MailAddress mailAddress;
            try
            {
                mailAddress = new MailAddress(mail);
            }
            catch (Exception)
            {
                Console.WriteLine("INVALID MAIL FORMAT ");
                return false;
            }
            string address = mailAddress.Address;
            if (CountOfDotAppreance(address) > 1) return false;
            if (CountOfAtAppreance(address) > 1) return false;
            if (!AreSpecialValidationsObeyed(address)) return false;
            if (address.Length < 3) return false;
            if (address.Length > 40) return false;
            if (!address.Contains(".")) return false;         
            return true;

        }


        public bool IsContentOfNameValid(string name, IMyCollection<Employee> myCollection)
        {
            if (name.Length < 2) return false;
            if (name.Length > 20) return false;
            if (name.Any(x => !char.IsLetter(x))) return false;
            return true;

        }

        public bool DoesMailAddressExist(string mail, IMyCollection<Employee> myCollection)
        {
            if (!IsContentOfTheMailValid(mail, myCollection)) return false;           
                var myIterator = myCollection.CreateIterator();
                while(myIterator.HasNext())
                {
                 if (myIterator.Current().EmailAddress.Address.Equals(mail)) return true;
                 myIterator.Next();
                }                 
             return false;
        }


        public bool AreSpecialValidationsObeyed(string address)
        {           
            if (address.IndexOf('.') < address.IndexOf('@')) return false;
            if (!AreAllCharactersValid(address)) return false;
            if (!IsFirstTwoCharactersValid(address.Substring(0, 2))) return false;
            if (DoesLettersAfterDotHasDigit(address)) return false;
            if (address.IndexOf('.') < address.IndexOf('@')) return false;
            return true;

        }


        public bool IsFirstTwoCharactersValid(string twoCharacters)
        {
            return twoCharacters.All(ch => char.IsLetter(ch));
        }


        public bool AreAllCharactersValid(string address)
        {
            return address.All(ch => char.IsLetter(ch)
            || char.IsDigit(ch) || ch == '@' || ch == '.');
        }

        public bool DoesLettersAfterDotHasDigit(string address)
        {
            return address.Substring(address.IndexOf('@') + 1).Any(ch => char.IsDigit(ch));
        }

        public bool DoesNameExists(string firstName, List<Employee> employeeList)
        {
            return employeeList.Any(emp => emp.FirstName.Equals(firstName));
        }


        public int CountOfDotAppreance(string address)
        {
            return address.Where(character => character == '.').Count();
        }

        public int CountOfAtAppreance(string address)
        {
            return address.Where(character => character == '@').Count();
        }

    }
}
