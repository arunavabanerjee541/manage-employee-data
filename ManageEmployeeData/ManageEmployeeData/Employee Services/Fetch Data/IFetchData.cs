﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ManageEmployeeData.Data_Management
{
    interface IFetchData
    {
        void FetchEmployeeDetailsByEmailAddress(string mail, IMyCollection<Employee> myCollection);
        void FetchAllEmployeeDetails(IMyCollection<Employee> myCollection);
    }
}
