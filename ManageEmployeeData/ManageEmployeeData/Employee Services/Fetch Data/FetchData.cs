﻿using ManageEmployeeData.Employee_Data_Services.Manipulate_Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ManageEmployeeData.Data_Management
{
    class FetchData : IFetchData
    {
        private DataManipulationHelper _dataManipulationHelper;
        public FetchData()
        {
            _dataManipulationHelper = new DataManipulationHelper();
        }

        public void FetchAllEmployeeDetails(IMyCollection<Employee> myCollection)
        {
           Iterator<Employee> iterator = myCollection.CreateIterator();
           while(iterator.HasNext())
            {
                Employee employee = iterator.Current();
                Print(employee);
                iterator.Next();
            }
        }

        public void FetchEmployeeDetailsByEmailAddress(string mail,IMyCollection<Employee> myCollection)
        {
            if (!_dataManipulationHelper.CheckIfMailAddressExists(mail, myCollection))
            {
                Console.WriteLine("Employee is not present with email address as " + mail);
                return;
            }      
                Iterator<Employee> iterator = myCollection.CreateIterator();
                while (iterator.HasNext())
                {
                     Employee employee = iterator.Current();
                     if (employee.EmailAddress.Address.Equals(mail))
                     {
                       Print(employee);
                       break;
                     }
                     iterator.Next();
                    
                }
                      
        }

        public void Print(Employee employee)
        {
            Console.WriteLine(
                "First Name = " +  employee.FirstName + " "
               +" Last Name = " + employee.LastName + " "
               +" Salary = " + employee.Salary + " "
               +" Email Address = " + employee.EmailAddress.Address);                               
        }



    }
}
