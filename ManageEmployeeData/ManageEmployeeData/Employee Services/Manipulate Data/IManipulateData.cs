﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ManageEmployeeData.Data_Management
{
    interface IManipulateData
    {
        void AddData(IMyCollection<Employee> myCollection, CollectionType typeOfCollection);
        void RemoveData(IMyCollection<Employee> myCollection, CollectionType typeOfCollection);
      
    }
}
