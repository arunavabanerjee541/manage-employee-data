﻿using ManageEmployeeData.Data_Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ManageEmployeeData.Employee_Data_Services.Manipulate_Data
{
    class DataManipulationHelper
    {
        private IValidateData _validate;
        private ValidationHelper _validationHelper;
        public DataManipulationHelper()
        {
            _validate = new Validate();
            _validationHelper = new ValidationHelper();
        }

        public bool CheckIfMailAddressExists(string mailAddress, IMyCollection<Employee> myCollection)
        {
            if (_validate.IsDataValid(mailAddress, _validationHelper.DoesMailAddressExist, myCollection))
                return true;

            return false;

        }

        public int GetEmployeeIndexInCollection(MailAddress mailAddress, IMyCollection<Employee> myCollection)
        {
            Iterator<Employee> iterator =myCollection.CreateIterator();
            int index = -1;
            while(iterator.HasNext())
            {
                index++;
                Employee employee = iterator.Current();
                if (employee.EmailAddress.Equals(mailAddress))
                    return index; ;

                iterator.Next();               
            }
            return index; ;

        }

        public Employee BuildDataToBeAdded(IMyCollection<Employee> myCollection)
        {
            Employee employee;
            Console.WriteLine("Please Enter first name ");
            string firstName = GetName(myCollection);
            Console.WriteLine("Please Enter second name ");
            string lastName = GetName(myCollection);
            MailAddress mail = GetMailAddress(myCollection);
            decimal salary = GetSalary();
            employee = new Employee(firstName, lastName, salary, mail);
            return employee;

        }


        public string GetName(IMyCollection<Employee> myCollection)
        {
            string name = Console.ReadLine();
            if (_validate.IsDataValid(name, _validationHelper.IsContentOfNameValid, myCollection))
            {
                return name;
            }
            else
            {
                Console.WriteLine(" Invalid name try again ");
                return GetName(myCollection);
            }
        }


        public MailAddress GetMailAddress(IMyCollection<Employee> myCollection)
        {
            Console.WriteLine("Please enter Employee Email Address ");
            string mail = Console.ReadLine();
            if (_validate.IsDataValid(mail, _validationHelper.IsContentOfTheMailValid, myCollection))
            {
                if (!_validate.IsDataValid(mail, _validationHelper.DoesMailAddressExist, myCollection))
                {
                    return new MailAddress(mail);
                }
                else
                {
                    Console.WriteLine(" This Mail Address Already Exists ");
                    return GetMailAddress(myCollection);
                }
            }
            else
            {
                Console.WriteLine(" invalid email address ");
                return GetMailAddress(myCollection);
            }
        }


        public decimal GetSalary()
        {
            Console.WriteLine(" Please enter Salary ");
            decimal salary = 0;
            try
            {
                salary = Convert.ToDecimal(Console.ReadLine());
                if (salary < 1)
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Invalid Salary ");
                return GetSalary();
            }
            return salary;
        }

    }
}
