﻿using ManageEmployeeData.Employee_Data_Services.Manipulate_Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ManageEmployeeData.Data_Management
{
    class ManipulateData: IManipulateData 
    {      
        private DataManipulationHelper _dataManipulationHelper;
        public ManipulateData()
        {
            _dataManipulationHelper = new DataManipulationHelper();
        }

        public void AddData(IMyCollection<Employee> myCollection, CollectionType typeOfCollection)
        {
            Employee employee =  _dataManipulationHelper.BuildDataToBeAdded(myCollection);
            bool isEmployeeAdded = false;
            switch (typeOfCollection)
            {
                case CollectionType.ArrayCollection:
                ArrayCollection <Employee> arrayCollection  = (ArrayCollection<Employee>)myCollection;
                if (arrayCollection.IsTotalItemLessThanMaxItemsAllowed())
                  {
                        arrayCollection.Add(employee);
                        isEmployeeAdded = true;
                  }
                else
                  {
                        isEmployeeAdded = false;
                        Console.WriteLine("Maximum Array Limit Exceeded");
                  }
                break;

                case CollectionType.ListCollection:
                ListCollection<Employee> listCollection = (ListCollection<Employee>)myCollection;
                listCollection.Add(employee);
                isEmployeeAdded = true;
                break;

                case CollectionType.DictionaryCollection:
                DictionaryCollection<MailAddress,Employee> dictinaryCollection = (DictionaryCollection<MailAddress,Employee>)myCollection;
                dictinaryCollection.Add(employee.EmailAddress, employee);
                isEmployeeAdded = true;
                break;
            }

            if(isEmployeeAdded)
                Console.WriteLine("Employee details Added" );
            
        }

        public void RemoveData(IMyCollection<Employee> myCollection, CollectionType typeOfCollection)
        {
            Console.WriteLine("Please Enter Employee Email Address you want to delete ");
            string mailAddress = Console.ReadLine();
            if( ! _dataManipulationHelper.CheckIfMailAddressExists(mailAddress,myCollection))
            {   
             Console.WriteLine("Employee not present with Email Address as " + mailAddress);
             return;
            }          
                int indexToBeRemoved = _dataManipulationHelper.GetEmployeeIndexInCollection(new MailAddress(mailAddress),myCollection);
                switch (typeOfCollection)
                {
                    case CollectionType.ArrayCollection:
                        ArrayCollection<Employee> arrayCollection = (ArrayCollection<Employee>)myCollection;
                        arrayCollection.Remove(indexToBeRemoved);
                        break;

                    case CollectionType.ListCollection:
                        ListCollection<Employee> listCollection = (ListCollection<Employee>)myCollection;
                        listCollection.Remove(indexToBeRemoved);
                        break;

                    case CollectionType.DictionaryCollection:
                        DictionaryCollection<MailAddress, Employee> dictinaryCollection = (DictionaryCollection<MailAddress, Employee>)myCollection;
                        dictinaryCollection.Remove(indexToBeRemoved);
                        break;
                }
            Console.WriteLine("Employee Deleted Successfully");          
            
        }

     


    }
}
