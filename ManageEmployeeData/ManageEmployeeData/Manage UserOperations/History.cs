﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageEmployeeData
{
    public class History<T>
    {
        private Dictionary<CollectionType, IMyCollection<T>> _historyOfCollectionsUsed;
        public History()
        {
            _historyOfCollectionsUsed = new Dictionary<CollectionType, IMyCollection<T>>();
        }

        public void AddToHistory(CollectionType collectionType,IMyCollection<T> myCollection)
        {
            _historyOfCollectionsUsed.Add(collectionType, myCollection);
        }

        public IMyCollection<T> GetCollectionFromHistory(CollectionType collectionType)
        {
            if (CheckIfCollectionExist(collectionType))
            {
            _historyOfCollectionsUsed.TryGetValue(collectionType, out IMyCollection<T> value);
                return value;
            }
            else
            {
                Console.WriteLine("No Collection Found");
                return null;
            }                      
        }

        public bool CheckIfCollectionExist(CollectionType collectionType)
        {
            return _historyOfCollectionsUsed.ContainsKey(collectionType) ? true : false;        
        }
    }
}
