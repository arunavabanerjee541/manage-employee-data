﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ManageEmployeeData
{
    class CaptureCollection
    {
        public static IMyCollection<Employee> GetCollectionObj(CollectionType typeOfCollection, History<Employee> history)
        {
            IMyCollection<Employee> myCollection = null;
            switch (typeOfCollection)
            {
                case CollectionType.ArrayCollection:
                    myCollection = GetArrayColllection(typeOfCollection, history);
                    break;
                case CollectionType.DictionaryCollection:
                    myCollection = GetDictionaryColllection(typeOfCollection, history);
                    break;
                case CollectionType.ListCollection:
                    myCollection = GetListColllection(typeOfCollection, history);
                    break;
            }
            return myCollection;
        }


        public static IMyCollection<Employee> GetArrayColllection(CollectionType typeOfCollection, History<Employee> history)
        {
            IMyCollection<Employee> myCollection = null;
            if (history.CheckIfCollectionExist(CollectionType.ArrayCollection))
                myCollection = history.GetCollectionFromHistory(CollectionType.ArrayCollection);
            else
            {
                myCollection = new ArrayCollection<Employee>();
                history.AddToHistory(CollectionType.ArrayCollection, myCollection);
            }
            return myCollection;
        }


        public static IMyCollection<Employee> GetListColllection(CollectionType typeOfCollection, History<Employee> history)
        {
            IMyCollection<Employee> myCollection = null;
            if (history.CheckIfCollectionExist(CollectionType.ListCollection))
                myCollection = history.GetCollectionFromHistory(CollectionType.ListCollection);
            else
            {
                myCollection = new ListCollection<Employee>();
                history.AddToHistory(CollectionType.ListCollection, myCollection);
            }
            return myCollection;
        }


        public static IMyCollection<Employee> GetDictionaryColllection(CollectionType typeOfCollection, History<Employee> history)
        {
            IMyCollection<Employee> myCollection = null;
            if (history.CheckIfCollectionExist(CollectionType.DictionaryCollection))
                myCollection = history.GetCollectionFromHistory(CollectionType.DictionaryCollection);
            else
            {
                myCollection = new DictionaryCollection<MailAddress, Employee>();
                history.AddToHistory(CollectionType.DictionaryCollection, myCollection);
            }
            return myCollection;
        }


    }
}
