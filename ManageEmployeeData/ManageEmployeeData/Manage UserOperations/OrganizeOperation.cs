﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageEmployeeData
{
    class OrganizeOperation
    {
        public static void ProceedWithOperations(Manager manager, IMyCollection<Employee> myCollection, CollectionType choiceOfCollection,UserInteraction userInteraction)
        {
            bool ValidOperationChoice = true;
            OperationType typeOfOperation;
            while (ValidOperationChoice)
            {
                typeOfOperation = userInteraction.ChooseOperationType();
                switch (typeOfOperation)
                {
                    case OperationType.AddAnEmployee:
                        manager.AddData(myCollection, choiceOfCollection);
                        break;
                    case OperationType.DeleteAnEmployee:
                        manager.RemoveData(myCollection, choiceOfCollection);
                        break;
                    case OperationType.GetDetailsOfAnEmployee:
                        manager.ShowEmployeeByEmailAddress(myCollection);
                        break;
                    case OperationType.GetDetailsOfAllEmoloyees:
                        manager.ShowAllEmployeeDetails(myCollection);
                        break;
                    default:
                        ValidOperationChoice = false;
                        break;
                }

            }

        }



    }
}
