﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageEmployeeData
{
    class UserInteraction
    {
        public CollectionType ChooseCollectionType()
        {
            ShowCollectionChoice();
            int lowerLimit = 1;
            int upperLimit = 3;
            int choice = MakeChoice();
            if (choice < lowerLimit || choice > upperLimit)
            {
                Console.WriteLine(" ******EXITING******");
                Environment.Exit(0);
            }
            int positionOfChosenCollection = choice - lowerLimit;
            CollectionType favourableColection = (CollectionType)positionOfChosenCollection;
            return favourableColection;
        }

        public void ShowCollectionChoice()
        {
            Console.WriteLine("Practice the Collections in C#");
            Console.WriteLine("Press 1 to implement Array");
            Console.WriteLine("Press 2 to implement List");
            Console.WriteLine("Press 3 to implement Dictionary");
            Console.WriteLine("Press any other Character to exit");
        }

        public OperationType ChooseOperationType()
        {
            ShowOperationChoice();
            int lowerLimit = 1;
            int upperLimit = 4;
            int choice = MakeChoice();
            if (choice < lowerLimit || choice > upperLimit)
                return OperationType.ExitFromTheCollection;

            int positionOfChosenOperation = choice - lowerLimit;
            OperationType favourableOperation = (OperationType)positionOfChosenOperation;
            return favourableOperation;

        }

        public void ShowOperationChoice()
        {
            Console.WriteLine("Press 1 to Add an Employee");
            Console.WriteLine("Press 2 to Delete an Employee");
            Console.WriteLine("Press 3 to get details an Employee");
            Console.WriteLine("Press 4 to get detatils of all Employee");
            Console.WriteLine("Press any other Character to exit from the Collections");

        }


        public int MakeChoice()
        {
            int choice = 0;
            try { choice = Convert.ToInt32(Console.ReadLine()); }
            catch (Exception)
            {
                Console.WriteLine(" Invalid Choice Try Again ");
                return MakeChoice();
            }
            return choice;
        }

    }
}
