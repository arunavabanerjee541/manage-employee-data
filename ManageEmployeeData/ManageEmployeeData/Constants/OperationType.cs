﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageEmployeeData
{
    public enum OperationType
    {
        AddAnEmployee,
        DeleteAnEmployee,
        GetDetailsOfAnEmployee,
        GetDetailsOfAllEmoloyees,
        ExitFromTheCollection
    }
}
