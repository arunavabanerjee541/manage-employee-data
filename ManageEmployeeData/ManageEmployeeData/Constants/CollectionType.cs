﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManageEmployeeData
{
    public enum CollectionType
    {
        ArrayCollection,
        ListCollection,
        DictionaryCollection
    }
}
