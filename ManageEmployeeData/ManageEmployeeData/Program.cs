﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ManageEmployeeData
{
    class Program
    {      
        static void Main(string[] args)
        {
             Manager manager = new Manager();
             History<Employee> history = new History<Employee>();
             UserInteraction userInteraction = new UserInteraction();
             IMyCollection<Employee> myCollection;
             bool ValidCollectionChoice = true;
             while(ValidCollectionChoice)
              {
                CollectionType choiceOfCollection = userInteraction.ChooseCollectionType();
                myCollection = CaptureCollection.GetCollectionObj(choiceOfCollection, history);
                OrganizeOperation.ProceedWithOperations(manager, myCollection,choiceOfCollection ,userInteraction);              
              }
        }     
    }
}
